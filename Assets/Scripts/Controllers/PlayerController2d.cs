﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2d : MonoBehaviour
{

    public float moveSpeed = 6;
    public float sprintFactor = 2;

    [Range(0.0f, 1f)]
    public float smoothingGround;

    [Range(0.0f, 1f)]
    public float smoothingAir;

    public float timeToJumpApex;
    public float maxJumpHeight;
    public float minJumpHeight;


    public PlayerAction[] actions;
    PowerSlam powerSlam;

    Animator anim;


    float zVelocitySmoothing;

    [HideInInspector]
    public PlayerInput input;
    [HideInInspector]
    public SidescollerControllerNew controller;
    [HideInInspector]
    public bool applyGravity = true;
    [HideInInspector]
    public float gravity;
    [HideInInspector]
    public float maxJumpVelocity;
    [HideInInspector]
    public float minJumpVelocity;
    [HideInInspector]
    public Vector3 velocity;

    bool inCombatStance;

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<SidescollerControllerNew>();
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
        input = new PlayerInput();
        powerSlam = GetComponent<PowerSlam>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();
        applyGravity = true;

        if (controller.collisionInfo.down || controller.collisionInfo.up)
        {
            velocity.y = 0f;
        }

        if (input.jump)
        {
            if (controller.collisionInfo.down)
            {
                Debug.Log("Should be jumping now. Y: " + velocity.y);
                velocity.y = maxJumpVelocity;
                anim.SetTrigger("Jump");
                Debug.Log("Jump should have been applied now. Y: " + velocity.y);
            }

        }

        if (input.jumpReleased)
        {
            if (velocity.y > minJumpVelocity)
            {
                velocity.y = minJumpVelocity;
            }
        }

        float targetVelocityZ = input.movementZ * moveSpeed;
        velocity.z = Mathf.SmoothDamp(velocity.z, targetVelocityZ, ref zVelocitySmoothing, (controller.collisionInfo.down ? smoothingGround : smoothingAir));

        if (powerSlam.inUse)
        {
            powerSlam.UpdateAbility();
        }

        if (applyGravity)
            velocity.y += gravity * Time.deltaTime;



        controller.Move(velocity * Time.deltaTime, input);
        anim.SetInteger("Speed", Mathf.Abs((int)(velocity.z / 3)));
        anim.SetBool("Grounded", controller.collisionInfo.down);
    }

    private void CheckInput()
    {
        input.Reset();
        for (int i = 0; i < actions.Length; i++)
        {
            if (Input.GetKeyDown(actions[i].button))
            {
                switch (actions[i].action)
                {
                    case Action.Jump:
                        {
                            input.jump = true;
                            break;
                        }

                    case Action.PowerSlam:
                        {
                            if (!controller.collisionInfo.down)
                            {
                                //input.powerSlam = true;
                                powerSlam.StartAbility(this);
                                anim.SetTrigger("PowerSlam");
                            }
                            break;
                        }

                    case Action.ResetPosition:
                        {
                            transform.position = new Vector3(0, 1, 0);
                            break;
                        }

                    case Action.ToggleStance:
                        {
                            inCombatStance = !inCombatStance;
                            anim.SetBool("Unsheathed", inCombatStance);
                            break;
                        }
                }
            }

            if (Input.GetKeyUp(actions[i].button))
            {
                switch (actions[i].action)
                {
                    case Action.Jump:
                        {
                            input.jumpReleased = true;
                            break;
                        }
                }
            }

            if (Input.GetKey(actions[i].button))
            {
                switch (actions[i].action)
                {
                    case Action.MoveRight:
                        {
                            input.movementZ = 1;
                            break;
                        }

                    case Action.MoveLeft:
                        {
                            input.movementZ = -1;
                            break;
                        }

                    case Action.Sprint:
                        {
                            input.movementZ *= sprintFactor;
                            break;
                        }
                }
            }
        }
    }

    [System.Serializable]
    public struct PlayerAction
    {
        public Action action;
        public KeyCode button;
    }

    public struct PlayerInput
    {
        public float movementZ;
        public bool jump, powerSlam, jumpReleased, stance;

        public bool noInput
        {
            get
            {
                return (movementZ == 0) && !jump && !powerSlam && !jumpReleased && !stance;
            }
        }


        public void Reset()
        {
            movementZ = 0;
            jump = false;
            powerSlam = false;
            jumpReleased = false;
            stance = false;
        }
    }

    [System.Serializable]
    public enum Action {
        Attack, Jump,
        MoveLeft, MoveRight,
        MoveFront, MoveBack,
        Dodge, Parry,
        Roll, PowerSlam,
        ResetPosition, Sprint,
        ToggleStance}
}


