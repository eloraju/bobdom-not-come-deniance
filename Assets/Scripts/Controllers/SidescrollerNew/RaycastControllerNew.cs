﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class RaycastControllerNew : MonoBehaviour
{
    [HideInInspector]
    public new Collider collider;

    float rayCount;

    public float skinWidth = .02f;
    float raySpacing = .1f;

    public LayerMask collisionMask;
    public float maxDescentAngle = 50;
    public float maxClimbAngle = 50;

    public bool debug;

    protected RaycastOrigins rayOrigins;
    protected float zRaySpacing, yRaySpacing, xRaySpacing;
    protected int zRayCount;
    protected int yRayCount;
    protected int xRayCount;

    private void Awake()
    {
        collider = GetComponent<Collider>();
        Init();
    }
    //There has to be a better way to do this...
    protected abstract void Init();

    public virtual void Start()
    {
        CalculateRayCount();
    }

    public void UpdateCollisions()
    {
        UpdateRayOrigins();
    }

    protected void UpdateRayOrigins()
    {
        Bounds bounds = collider.bounds;

        rayOrigins.bottomBackLeft = new Vector3(bounds.min.x, bounds.min.y + skinWidth, bounds.min.z);
        rayOrigins.bottomFrontLeft = new Vector3(bounds.min.x, bounds.min.y + skinWidth, bounds.max.z);
        rayOrigins.bottomBackRight = new Vector3(bounds.max.x, bounds.min.y + skinWidth, bounds.min.z);
        rayOrigins.bottomFrontRight = new Vector3(bounds.max.x, bounds.min.y + skinWidth, bounds.max.z);

        rayOrigins.frontBottomLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z - skinWidth);
        rayOrigins.frontBottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z - skinWidth);
        rayOrigins.frontTopLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z - skinWidth);
        rayOrigins.frontTopRight = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z - skinWidth);

        rayOrigins.topBackLeft = new Vector3(bounds.min.x, bounds.max.y - skinWidth, bounds.min.z);
        rayOrigins.topFrontLeft = new Vector3(bounds.min.x, bounds.max.y - skinWidth, bounds.max.z);
        rayOrigins.topBackRight = new Vector3(bounds.max.x, bounds.max.y - skinWidth, bounds.min.z);
        rayOrigins.topFrontRight = new Vector3(bounds.max.x, bounds.max.y - skinWidth, bounds.max.z);

        rayOrigins.backBottomLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z + skinWidth);
        rayOrigins.backBottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z + skinWidth);
        rayOrigins.backTopLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z + skinWidth);
        rayOrigins.backTopRight = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z + skinWidth);

        rayOrigins.rightBottomFront = new Vector3(bounds.max.x - skinWidth, bounds.min.y, bounds.max.z);
        rayOrigins.rightBottomBack = new Vector3(bounds.max.x - skinWidth, bounds.min.y, bounds.min.z);
        rayOrigins.rightTopFront = new Vector3(bounds.max.x - skinWidth, bounds.max.y, bounds.max.z);
        rayOrigins.rightTopBack = new Vector3(bounds.max.x - skinWidth, bounds.max.y, bounds.min.z);

        rayOrigins.leftBottomFront = new Vector3(bounds.min.x + skinWidth, bounds.min.y, bounds.max.z);
        rayOrigins.leftBottomBack = new Vector3(bounds.min.x + skinWidth, bounds.min.y, bounds.min.z);
        rayOrigins.leftTopFront = new Vector3(bounds.min.x + skinWidth, bounds.max.y, bounds.max.z);
        rayOrigins.leftTopBack = new Vector3(bounds.min.x + skinWidth, bounds.max.y, bounds.min.z);

        rayOrigins.bottomCenter = rayOrigins.topCenter = bounds.center;
        rayOrigins.bottomCenter.y = bounds.min.y;
        rayOrigins.topCenter.y = bounds.max.y;
    }

    private void CalculateRayCount()
    {
        Bounds bounds = collider.bounds;

        float boundsWidth, boundsDepth, boundsHeight;
        boundsWidth = bounds.size.z - (2 * skinWidth);
        boundsDepth = bounds.size.x;
        boundsHeight = bounds.size.y - (2 * skinWidth);

        yRayCount = Mathf.RoundToInt(boundsHeight / raySpacing);
        xRayCount = Mathf.RoundToInt(boundsDepth / raySpacing);
        zRayCount = Mathf.RoundToInt(boundsWidth / raySpacing);

        yRaySpacing = bounds.size.y / (yRayCount - 1);
        xRaySpacing = bounds.size.x / (xRayCount - 1);
        zRaySpacing = bounds.size.z / (zRayCount - 1);
    }


    /**
     * Cast rays all rays from the given face. If we hit something, we call the provided handler 'hitHandler'
     * The handler should return the new rayLenght if one is needed
     */
    protected void CastRaysFromFace(RaycastFace face, float initRayLenght, System.Func<RaycastHit, float> hitHandler)
    {

        int firstAxisRayCount, secondAxisRayCount;
        float firstAxisRaySpacing, secondAxisRaySpacing;
        Vector3 rayDirection, firstAxisAdditionDirection, secondAxisAdditionDirection;
        Vector3 initRayOrigin;

        switch (face)
        {
            case RaycastFace.FORWARD:
                {
                    firstAxisRayCount = xRayCount;
                    secondAxisRayCount = yRayCount;

                    firstAxisRaySpacing = xRaySpacing;
                    secondAxisRaySpacing = yRaySpacing;

                    rayDirection = Vector3.forward;
                    initRayOrigin = rayOrigins.frontBottomLeft;

                    firstAxisAdditionDirection = Vector3.right;
                    secondAxisAdditionDirection = Vector3.up;
                    break;
                }

            case RaycastFace.BACK:
                {
                    firstAxisRayCount = xRayCount;
                    secondAxisRayCount = yRayCount;

                    firstAxisRaySpacing = xRaySpacing;
                    secondAxisRaySpacing = yRaySpacing;

                    rayDirection = Vector3.back;
                    initRayOrigin = rayOrigins.backBottomLeft;

                    firstAxisAdditionDirection = Vector3.right;
                    secondAxisAdditionDirection = Vector3.up;
                    break;
                }

            case RaycastFace.LEFT:
                {
                    firstAxisRayCount = zRayCount;
                    secondAxisRayCount = yRayCount;

                    firstAxisRaySpacing = zRaySpacing;
                    secondAxisRaySpacing = yRaySpacing;

                    rayDirection = Vector3.left;
                    //TODO: Add ray origins for LEFT and RIGHT
                    initRayOrigin = Vector3.zero;

                    firstAxisAdditionDirection = Vector3.forward;
                    secondAxisAdditionDirection = Vector3.up;
                    break;
                }

            case RaycastFace.RIGHT:
                {
                    firstAxisRayCount = zRayCount;
                    secondAxisRayCount = yRayCount;

                    firstAxisRaySpacing = zRaySpacing;
                    secondAxisRaySpacing = yRaySpacing;

                    rayDirection = Vector3.right;
                    //TODO: Add ray origins for LEFT and RIGHT
                    initRayOrigin = Vector3.zero;

                    firstAxisAdditionDirection = Vector3.forward;
                    secondAxisAdditionDirection = Vector3.up;
                    break;
                }

            case RaycastFace.UP:
                {
                    firstAxisRayCount = xRayCount;
                    secondAxisRayCount = zRayCount;

                    firstAxisRaySpacing = xRaySpacing;
                    secondAxisRaySpacing = zRaySpacing;

                    rayDirection = Vector3.up;
                    initRayOrigin = rayOrigins.topBackLeft;

                    firstAxisAdditionDirection = Vector3.right;
                    secondAxisAdditionDirection = Vector3.forward;
                    break;
                }

            case RaycastFace.DOWN:
                {
                    firstAxisRayCount = xRayCount;
                    secondAxisRayCount = zRayCount;

                    firstAxisRaySpacing = xRaySpacing;
                    secondAxisRaySpacing = zRaySpacing;

                    rayDirection = Vector3.down;
                    initRayOrigin = rayOrigins.bottomBackLeft;

                    firstAxisAdditionDirection = Vector3.right;
                    secondAxisAdditionDirection = Vector3.forward;
                    break;
                }

            default:
                {
                    firstAxisRayCount = 0;
                    secondAxisRayCount = 0;

                    firstAxisRaySpacing = 0;
                    secondAxisRaySpacing = 0;

                    rayDirection = Vector3.zero;
                    initRayOrigin = Vector3.zero;

                    firstAxisAdditionDirection = Vector3.right;
                    secondAxisAdditionDirection = Vector3.forward;
                    break;
                }
        }


        float rayLenght = Mathf.Abs(initRayLenght) + skinWidth;

        for (int firstAxis = 0; firstAxis < firstAxisRayCount; firstAxis++)
        {
            for (int secondAxis = 0; secondAxis < secondAxisRayCount; secondAxis++)
            {
                Vector3 rayOrigin = initRayOrigin;
                rayOrigin += (firstAxisAdditionDirection * firstAxisRaySpacing * firstAxis) + (secondAxisAdditionDirection * secondAxisRaySpacing * secondAxis);
                RaycastHit hitInfo;

                if (debug)
                    Debug.DrawRay(rayOrigin, rayDirection, Color.green);

                //TODO: Collision mask needs to be a parameter
                if (Physics.Raycast(rayOrigin, rayDirection, out hitInfo, rayLenght, collisionMask))
                {
                    //If we hit something we call the handler function
                    rayLenght = hitHandler(hitInfo);
                }
            }
        }
    }

    protected void CastRaysFromEdge(RaycastEdge edge, Vector3 direction, float initRayLenght, System.Func<RaycastHit, float> hitHandler)
    {
        float rayLenght = Mathf.Abs(initRayLenght) + skinWidth;
        Vector3 rayDirection = direction;

        Vector3 initRayOrigin, rayAdditionDirection;
        int rayCount;
        float raySpacing;

        switch (edge)
        {
            case RaycastEdge.X_BOTTOM_FRONT:
            case RaycastEdge.X_BOTTOM_BACK:
            case RaycastEdge.X_TOP_FRONT:
            case RaycastEdge.X_TOP_BACK:
                {
                    rayCount = xRayCount;
                    raySpacing = xRaySpacing;
                    rayAdditionDirection = Vector3.right;
                    break;
                }

            case RaycastEdge.Z_BOTTOM_RIGHT:
            case RaycastEdge.Z_BOTTOM_LEFT:
            case RaycastEdge.Z_TOP_RIGHT:
            case RaycastEdge.Z_TOP_LEFT:
                {
                    rayCount = zRayCount;
                    raySpacing = zRaySpacing;
                    rayAdditionDirection = Vector3.forward;
                    break;
                }

            case RaycastEdge.Y_FRONT_LEFT:
            case RaycastEdge.Y_BACK_RIGHT:
            case RaycastEdge.Y_FRONT_RIGHT:
            case RaycastEdge.Y_BACK_LEFT:
                {
                    rayCount = yRayCount;
                    raySpacing = yRaySpacing;
                    rayAdditionDirection = Vector3.up;
                    break;
                }

            default:
                {
                    rayCount = 0;
                    raySpacing = 0;
                    rayAdditionDirection = Vector3.zero;
                    break;
                }
        }

        switch (edge)
        {
            case RaycastEdge.X_BOTTOM_FRONT:
                {
                    initRayOrigin = direction == Vector3.down ? rayOrigins.bottomFrontLeft : rayOrigins.frontBottomLeft;
                    break;
                }
            case RaycastEdge.X_BOTTOM_BACK:
                {
                    initRayOrigin = direction == Vector3.down ? rayOrigins.bottomBackLeft : rayOrigins.backBottomLeft;
                    break;
                }

            case RaycastEdge.X_TOP_FRONT:
                {
                    initRayOrigin = direction == Vector3.up ? rayOrigins.topFrontLeft : rayOrigins.frontTopLeft;
                    break;
                }

            case RaycastEdge.X_TOP_BACK:
                {
                    initRayOrigin = direction == Vector3.up ? rayOrigins.topBackLeft : rayOrigins.backTopLeft;
                    break;
                }

            case RaycastEdge.Z_BOTTOM_RIGHT:
                {
                    initRayOrigin = direction == Vector3.down ? rayOrigins.bottomBackRight : rayOrigins.backBottomRight;
                    break;
                }

            case RaycastEdge.Z_BOTTOM_LEFT:
                {
                    initRayOrigin = direction == Vector3.down ? rayOrigins.bottomBackLeft : rayOrigins.backBottomLeft;
                    break;
                }

            case RaycastEdge.Z_TOP_RIGHT:
                {
                    initRayOrigin = direction == Vector3.up ? rayOrigins.topBackRight : rayOrigins.rightTopBack;
                    break;
                }

            case RaycastEdge.Z_TOP_LEFT:
                {
                    initRayOrigin = direction == Vector3.up ? rayOrigins.topBackLeft : rayOrigins.leftTopBack;
                    break;
                }

            case RaycastEdge.Y_FRONT_RIGHT:
                {
                    initRayOrigin = direction == Vector3.forward ? rayOrigins.frontBottomRight : rayOrigins.rightBottomFront;
                    break;
                }

            case RaycastEdge.Y_FRONT_LEFT:
                {
                    initRayOrigin = direction == Vector3.forward ? rayOrigins.frontBottomLeft : rayOrigins.leftBottomFront;
                    break;
                }

            case RaycastEdge.Y_BACK_RIGHT:
                {
                    initRayOrigin = direction == Vector3.back ? rayOrigins.backBottomRight : rayOrigins.rightBottomBack;
                    break;
                }

            case RaycastEdge.Y_BACK_LEFT:
                {
                    initRayOrigin = direction == Vector3.back ? rayOrigins.backBottomLeft : rayOrigins.leftBottomBack;
                    break;
                }

            default:
                {
                    initRayOrigin = Vector3.zero;
                    break;
                }
        }

        for (int currentRayNumber = 0; currentRayNumber < rayCount; currentRayNumber++)
        {

            Vector3 rayOrigin = initRayOrigin;
            rayOrigin += (rayAdditionDirection * raySpacing * currentRayNumber);
            RaycastHit hitInfo;

            if (debug)
                Debug.DrawRay(rayOrigin, rayDirection, Color.red);

            //TODO: Collision mask needs to be a parameter
            if (Physics.Raycast(rayOrigin, rayDirection, out hitInfo, rayLenght, collisionMask))
            {
                //If we hit something we call the handler function
                rayLenght = hitHandler(hitInfo);
            }
        }

    }

    protected enum RaycastFace
    {
        FORWARD, BACK, LEFT, RIGHT, UP, DOWN
    }

    protected enum RaycastEdge
    {
        X_BOTTOM_BACK, X_BOTTOM_FRONT, X_TOP_BACK, X_TOP_FRONT,
        Z_BOTTOM_LEFT, Z_BOTTOM_RIGHT, Z_TOP_LEFT, Z_TOP_RIGHT,
        Y_FRONT_LEFT, Y_FRONT_RIGHT, Y_BACK_LEFT, Y_BACK_RIGHT
    }



    protected struct RaycastOrigins
    {
        public Vector3 bottomBackLeft, bottomFrontLeft, bottomBackRight, bottomFrontRight;
        public Vector3 topBackLeft, topFrontLeft, topBackRight, topFrontRight;
        public Vector3 frontTopLeft, frontBottomLeft, frontTopRight, frontBottomRight;
        public Vector3 backTopLeft, backBottomLeft, backTopRight, backBottomRight;
        public Vector3 rightBottomFront, rightBottomBack, rightTopFront, rightTopBack;
        public Vector3 leftBottomFront, leftBottomBack, leftTopBack, leftTopFront;
        public Vector3 bottomCenter, topCenter;
    }
}
