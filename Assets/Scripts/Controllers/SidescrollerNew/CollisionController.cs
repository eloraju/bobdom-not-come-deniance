﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : RaycastControllerNew
{

    public DetailedCollisionInfo collisionInfo;

    protected override void Init()
    {
        collisionInfo = new DetailedCollisionInfo();
    }

    protected void UpdateCollisionOrigins()
    {
        UpdateRayOrigins();
        collisionInfo.Reset();
    }

    protected void UpdateCollisionsBelow(float initRayLenght)
    {
        CastRaysFromFace(RaycastFace.DOWN, initRayLenght, hit =>
        {
            collisionInfo.down = true;
            collisionInfo.distDown = hit.distance;
            collisionInfo.angleDown = Vector3.Angle(hit.normal, Vector3.up);
            collisionInfo.tagDown = hit.collider.tag;
            return hit.distance;
        });
    }

    protected void UpdateCollisionsAbove(float initRayLenght)
    {
        CastRaysFromFace(RaycastFace.UP, initRayLenght, hit =>
       {
           collisionInfo.up = true;
           collisionInfo.distUp = hit.distance;
           collisionInfo.angleUp = Vector3.Angle(hit.normal, Vector3.up);
           collisionInfo.tagUp = hit.collider.tag;
           return hit.distance;
       });
    }

    protected void UpdateCollisionsAhead(float initRayLenght)
    {
        CastRaysFromFace(RaycastFace.FORWARD, initRayLenght, hit =>
       {
           collisionInfo.forward = true;
           collisionInfo.distForward = hit.distance;
           collisionInfo.angleForward = Vector3.Angle(hit.normal, Vector3.up);
           collisionInfo.onSlope = collisionInfo.angleForward != 0;
           collisionInfo.tagForward = hit.collider.tag;
           return hit.distance;
       });
    }

    protected void UpdateCollisionsBehind(float initRayLenght)
    {
        CastRaysFromFace(RaycastFace.BACK, initRayLenght, hit =>
       {
           collisionInfo.back = true;
           collisionInfo.distBack = hit.distance;
           collisionInfo.angleBack = Vector3.Angle(hit.normal, Vector3.up);
           collisionInfo.onSlope = collisionInfo.angleBack != 0;
           collisionInfo.tagBack = hit.collider.tag;
           return hit.distance;
       });
    }

    protected void UpdateCollisionsLeft(float initRayLenght)
    {
        CastRaysFromFace(RaycastFace.LEFT, initRayLenght, hit =>
       {
           collisionInfo.left = true;
           collisionInfo.distLeft = hit.distance;
           collisionInfo.angleLeft = Vector3.Angle(hit.normal, Vector3.up);
           collisionInfo.onSlope = collisionInfo.angleLeft != 0;
           collisionInfo.tagLeft = hit.collider.tag;
           return hit.distance;
       });
    }

    protected void UpdateCollisionsRight(float initRayLenght)
    {
        CastRaysFromFace(RaycastFace.RIGHT, initRayLenght, hit =>
        {
            collisionInfo.right = true;
            collisionInfo.distRight = hit.distance;
            collisionInfo.angleRight = Vector3.Angle(hit.normal, Vector3.up);
            collisionInfo.onSlope = collisionInfo.angleRight != 0;
            collisionInfo.tagRight = hit.collider.tag;
            return hit.distance;
        });
    }

    protected void UpdateSlopeCollisionFromBottomBackEdge(float initRayLenght)
    {
        CastRaysFromEdge(RaycastEdge.X_BOTTOM_BACK, Vector3.down, initRayLenght, hit =>
         {
              
             return 0;
         });
    }

    [System.Serializable]
    public struct DetailedCollisionInfo
    {
 
        
        public bool left, right;
        public bool up, down;
        public bool forward, back;
        public bool onSlope, descendingSlope, climbingSlope;

        public float angleLeft, angleRight;
        public float angleUp, angleDown;
        public float angleForward, angleBack;

        public float distLeft, distRight;
        public float distForward, distBack;
        public float distUp, distDown;

        public string tagLeft, tagRight;
        public string tagUp, tagDown;
        public string tagForward, tagBack;

        public void Reset()
        {
            left = right = false;
            up = down = false;
            forward = back = false;
            onSlope = descendingSlope = climbingSlope = false;

            distLeft = distRight = 0;
            distForward = distBack = 0;
            distUp = distDown = 0;

            angleLeft = angleRight = 0;
            angleUp = angleDown = 0;
            angleForward = angleBack = 0;

            tagLeft = tagRight = null;
            tagUp = tagDown = null;
            tagForward = tagBack = null;
        }
    }


}
