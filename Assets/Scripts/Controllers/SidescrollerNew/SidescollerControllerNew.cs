﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SidescollerControllerNew : CollisionController
{


    private Vector3 moveVector;
    public PlayerController2d.PlayerInput input;

    public void Move(Vector3 newMoveVector, PlayerController2d.PlayerInput input)
    {
        moveVector = newMoveVector;
        this.input = input;
        UpdateCollisionOrigins();

        //If we are standing still or moving down
        if (moveVector.y <= 0)
            UpdateCollisionsBelow(moveVector.y);

        //If we are moving up
        if (moveVector.y > 0)
            UpdateCollisionsAbove(moveVector.y);

        //If we are movinb left or right
        if (moveVector.z != 0)
        {
            if (moveVector.z > 0)
                UpdateCollisionsAhead(moveVector.z);
            else
                UpdateCollisionsBehind(moveVector.z);
        }

        //if (moveVector.z != 0 && moveVector.y != 0 && !input.jump)


        Debug.Log("Move vector before modification: " + moveVector);
        ConstructMoveVector();
        Debug.Log("Move vector after modification: " + moveVector);
        transform.Translate(moveVector);
    }


    private void ConstructMoveVector()
    {
        if (collisionInfo.onSlope)
        {
            ConstructSlopeMoveVector();
        }
        else if (collisionInfo.down)
        {
            ConstructGroundedMoveVector();
        }
        else
        {
            ConstructInAirMoveVector();
        }
    }

    private void ConstructSlopeMoveVector()
    {

    }

    private void ConstructGroundedMoveVector()
    {
        moveVector.y = (collisionInfo.distDown - skinWidth) * Mathf.Sign(moveVector.y);
        if (collisionInfo.back || collisionInfo.forward)
            moveVector.z = ((collisionInfo.forward ? collisionInfo.distForward : collisionInfo.distBack) - skinWidth) * Mathf.Sign(moveVector.z);
    }

    private void ConstructInAirMoveVector()
    {

    }

    /**
     * Check wheter we are touching ground or not
     * */
    private void IsGrounded()
    {

        //If we hit something, we want to set our movement vector Y to equal the distance between
        //the player and the ground so we don't fall trough.


    }
}
