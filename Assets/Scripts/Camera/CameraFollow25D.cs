﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow25D : MonoBehaviour
{

    public SidescollerControllerNew target;
    public Vector3 focusAreaSize;
    public float cameraDistance;

    public float verticalOffset;
    public float lookAheadDstZ, lookAheadDstY;
    public float lookSmoothTimeZ, lookSmoothTimeY;

    public bool debug;

    FocusArea focusArea;

    float currentLookAheadZ, currentLookAheadY;
    float targetLookaAheadZ, targetLookAheadY;
    float lookAheadDirZ, lookAheadDirY;
    float smoothLookVelocityZ, smoothLookVelocityY;

    bool lookAheadStopped;

    private void Start()
    {
        focusArea = new FocusArea(target.collider.bounds, focusAreaSize);
    }

    private void LateUpdate()
    {
        focusArea.Update(target.collider.bounds);

        Vector3 focusPosition = focusArea.center + Vector3.up * verticalOffset;

        if (focusArea.velocity.z != 0)
        {
            lookAheadDirZ = Mathf.Sign(focusArea.velocity.z);
            if(Mathf.Sign(target.input.movementZ) == Mathf.Sign(focusArea.velocity.z) && target.input.movementZ != 0)
            {
                lookAheadStopped = false;
                targetLookaAheadZ = lookAheadDirZ * lookAheadDstZ;
            }
            else
            {
                if (!lookAheadStopped)
                {
                    targetLookaAheadZ = currentLookAheadZ + (lookAheadDirZ * lookAheadDstZ - currentLookAheadZ) / 4f;
                    lookAheadStopped = true;
                }
            }
        }

        
        currentLookAheadZ = Mathf.SmoothDamp(currentLookAheadZ, targetLookaAheadZ, ref smoothLookVelocityZ, lookSmoothTimeZ);
        focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothLookVelocityY, lookSmoothTimeY);
        focusPosition += Vector3.forward * currentLookAheadZ;

        transform.position = focusPosition + Vector3.right * cameraDistance;
    }

    private void OnDrawGizmos()
    {
        if (debug)
        {
            Gizmos.color = new Color(1, 0, 0, 0.4f);
            Gizmos.DrawCube(focusArea.center, focusAreaSize);
        }

    }

    struct FocusArea
    {
        public Vector3 center;
        public Vector3 velocity;
        public float back, front;
        public float left, right;
        public float top, bottom;

        public FocusArea(Bounds targetBounds, Vector3 size)
        {
            top = targetBounds.min.y + size.y;
            bottom = targetBounds.min.y;
            left = targetBounds.center.x - size.x / 2;
            right = targetBounds.center.x + size.x / 2;
            front = targetBounds.center.z + size.z / 2;
            back = targetBounds.center.z - size.z / 2;

            velocity = Vector3.zero;
            center = new Vector3((left + right) / 2, (top + bottom) / 2, (front + back) / 2);
        }

        public void Update(Bounds targetBounds)
        {
            float shiftZ = 0;
            if (targetBounds.min.z < back)
            {
                shiftZ = targetBounds.min.z - back;
            }
            else if (targetBounds.max.z > front)
            {
                shiftZ = targetBounds.max.z - front;
            }
            front += shiftZ;
            back += shiftZ;


            float shiftY = 0;
            if (targetBounds.min.y < bottom)
            {
                shiftY = targetBounds.min.y - bottom;
            }
            else if (targetBounds.max.y > top)
            {
                shiftY = targetBounds.max.y - top;
            }
            top += shiftY;
            bottom += shiftY;

            center = new Vector3((left + right) / 2, (top + bottom) / 2, (front + back) / 2);
            velocity = new Vector3(0, shiftY, shiftZ);
        }
    }
}
