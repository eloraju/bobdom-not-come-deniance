﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Weapon : MonoBehaviour {

    protected Rigidbody rb;
    public float damage;
    public float attackSpeed;
    public GameObject hitBox;

    private List<string> alreadyHit = new List<string>();

    protected bool attacking = false;

	void Awake () {
        rb = GetComponent<Rigidbody>();
        hitBox.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision detected!");
        //Create a class that holds all character info (health, stamina etc.) and get that from the collider hierarchy. other.GetComponentInParent<Unit>();
        if (other.tag == "Enemy" && !alreadyHit.Contains(other.name))
        {
            Debug.Log("Hit an enemy! Name: " + other.name);
            alreadyHit.Add(other.name);
            //This will crash the system if the enemy doesn't have a Unit script attached to it. Every enemy should require a Unit class so we know that they will be able to take damage.
            other.GetComponent<Unit>().TakeHit(damage);
        }
    }

    public void StartAttack()
    {
        hitBox.SetActive(true);
        alreadyHit.Clear();
    }

    public void EndAttack()
    {
        hitBox.SetActive(false);
        alreadyHit.Clear();
    }
}
