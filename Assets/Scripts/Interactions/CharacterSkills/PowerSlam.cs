﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerSlam : Ability
{

    [Header("Hang variables")]
    public float hangTime = 500;
    public float powerSlamGravityMult = 1.1f;
    public float playerSlowdownY = 1.5f;
    public float playerSlowdownZ = 1.2f;
    float timer;
    float cVelY;

    protected override void PrepareSkill()
    {
        //Convert Time.deltaTime to millisecods. Fucking Unity...
        //Start the timer by setting the timer value to our hang time
        timer = hangTime / 1000;
        inUse = true;
    }

    public override void Clean()
    {
        player = null;
    }

    public override void UpdateAbility()
    {
        if (timer > 0)
        {
            player.velocity.y /= playerSlowdownY;
            player.velocity.z /= playerSlowdownZ;
            player.applyGravity = false;
            timer -= Time.deltaTime;
        }
        else if (timer <= 0 && !player.controller.collisionInfo.down)
        {
            player.velocity.y += player.gravity * powerSlamGravityMult * Time.deltaTime;
        }
        else
        {
            inUse = false;
            Clean();
        }
    }
}
