﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Ability : MonoBehaviour {

    protected PlayerController2d player;
    public new string name;
    public bool canUse, inUse;

    public void StartAbility(PlayerController2d pl)
    {
        player = pl;
        PrepareSkill();
        inUse = true;
    }

    public void EndAbility()
    {
        inUse = false;
        Clean();
    }

    protected abstract void PrepareSkill();
    public abstract void UpdateAbility();
    public abstract void Clean();

}
