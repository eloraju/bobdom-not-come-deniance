﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{

    public float maxHealth = 100f;
    public float curHealth;

    public void TakeHit(float damage)
    {
        if (curHealth > 0)
        {
            curHealth -= damage;
            Debug.Log(gameObject.name + " took " + damage + " damage.");
        }
        else
        {
            Debug.Log(gameObject.name + "is already dead :(");
        }


        if (curHealth <= 0)
        {
            curHealth = 0;
            Debug.Log(gameObject.name+" should die now.");
        }

    }

    public void Heal(float damage)
    {
        if (curHealth + damage >= maxHealth)
        {
            curHealth = maxHealth;
        }
        else
        {
            curHealth += damage;
        }
    }


}
