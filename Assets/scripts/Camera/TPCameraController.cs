﻿using UnityEngine;

public class TPCameraController : MonoBehaviour
{

    public Transform target, tpCamera;
    private Transform cameraTarget;
    private GameObject cameraTargetObject;

    public float heightAboveTarget = 0.5f;

    private float mouseX, mouseY;
    public float mouseSensitivity = 10f;
    public float mouseYPosition = 1f;

    private float zoom;
    public float zoomSpeed = 2;
    public float zoomMin = -2;
    public float zoomMax = -10;

    public bool invertX = false; 
    public bool invertY = true;

    void Start()
    {
        zoom = zoomMin;
        cameraTargetObject = new GameObject("cameraTarget");
        cameraTarget = cameraTargetObject.transform;
        cameraTarget.position = new Vector3(target.position.x, target.lossyScale.y + heightAboveTarget, target.position.z);
        tpCamera.parent = cameraTarget;
    }

    void LateUpdate()
    {
        checkZoom();
        checkMouseMovement();
        moveCamera();
    }

    private void checkZoom()
    {
        zoom += Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
        if (zoom > zoomMin)
            zoom = zoomMin;

        if (zoom < zoomMax)
            zoom = zoomMax;

        tpCamera.transform.localPosition = new Vector3(0, 0, zoom);
    }

    private void checkMouseMovement()
    {
        mouseX += invertX ? Input.GetAxis("Mouse X") : -1 * Input.GetAxis("Mouse X");
        mouseY -= invertY ? Input.GetAxis("Mouse Y") : -1 * Input.GetAxis("Mouse Y");

        mouseY = Mathf.Clamp(mouseY, -10, 60);

        tpCamera.LookAt(cameraTarget);
        cameraTarget.localRotation = Quaternion.Euler(mouseY, mouseX, 0);
    }

    private void moveCamera()
    {
        cameraTarget.position = new Vector3(target.position.x, target.position.y + mouseYPosition + heightAboveTarget, target.position.z);
    }
}
