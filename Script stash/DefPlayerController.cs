﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TPCameraController))]
public class DefPlayerController : ThirdPersonController
{

    public TPCameraController cameraController;

    float jumpHoldTime, attackHoldTime, lockOnHold;
    bool lockOn, inAir, attacking;

    public float AttackDuration;
    private float attackElapsed;

    private int curAttack;

    public GameObject lockOnTarget;

    int attackHash = Animator.StringToHash("Attack");
    int jumpHash = Animator.StringToHash("Jump");
    int attack2Hash = Animator.StringToHash("Attack2");

    public override void ConsumeInput(InputData data)
    {
        ResetMovement();

        if (data.axis[0] != 0f)
        {
            xVelocity = data.axis[0] * walkSpeed;
            anim.SetBool("Walking", true);
        }

        if (data.axis[1] != 0f)
        {
            zVelocity = data.axis[1] * walkSpeed;
            anim.SetBool("Walking", true);
        }

        if (data.buttons[0])
        {
            if (jumpHoldTime == 0f && grounded)
            {
                yVelocity += jumpSpeed;
                anim.SetTrigger(jumpHash);
            }
            jumpHoldTime += Time.deltaTime;
        }
        else
        {
            jumpHoldTime = 0f;
        }


        if (data.buttons[1] && !attacking)
        {
            if (attackHoldTime == 0f)
            {
                weapons[equippedWeapon].StartAttack();
                attacking = true;
                anim.SetTrigger(attackHash);
                curAttack = 1;
            }
            attackHoldTime += Time.deltaTime;
        }
        else
        {
            attackHoldTime = 0f;
        }

        //lockOn = overrideTurn = data.buttons[2];
        if (data.buttons[2])
        {
            if (lockOnHold == 0)
            {
                lockOn = !lockOn;
            }
            lockOnHold += Time.deltaTime;
        }
        else
        {
            lockOnHold = 0;
        }

        if (lockOn)
        {
            overrideTurn = true;
            turnOverride = lockOnTarget.transform;
        }

        newInstructions = true;
    }

    protected override void ResetInput()
    {
        if (grounded)
            ResetMovement();
        ResetHolds();
        if (attacking)
        {
            CheckAttackTimers();
        }

    }

    private void ResetMovement()
    {
        xVelocity = 0;
        zVelocity = 0;
        yVelocity = 0;
        anim.SetBool("Walking", false);
        overrideTurn = false;
    }

    private void ResetHolds()
    {
        jumpHoldTime = 0f;
        attackHoldTime = 0f;
    }

    private void CheckAttackTimers()
    {
        if (attackElapsed < AttackDuration)
        {
            attackElapsed += Time.deltaTime;
        }
        else
        {
            weapons[equippedWeapon].EndAttack();
        }
    }
}
