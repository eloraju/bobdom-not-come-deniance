﻿using UnityEngine;
using System.Collections;

public class KeyboardAndMouseListener : InputListener
{
    public AxisButtons[] axisKeys;
    public ActionButtons[] buttonKeys;

    private void Reset()
    {
        inputManager = GetComponent<InputManager>();
        buttonKeys = new ActionButtons[inputManager.buttonCount];
        axisKeys = new AxisButtons[inputManager.axisCount];
    }

    void Update()
    {
        for (int i = 0; i < buttonKeys.Length; i++)
        {
            if (Input.GetKey(buttonKeys[i].button))
            {
                data.buttons[i] = true;
                newData = true;
            }
        }

        for (int i = 0; i < axisKeys.Length; i++)
        {
           //float axisVal = 0f;

            if (Input.GetKey(axisKeys[i].positivite))
            {
                data.axis[i] += 1;
                newData = true;
            }

            if (Input.GetKey(axisKeys[i].negative))
            {
                data.axis[i] -= 1;
                newData = true;
            }

            //data.axis[i] -= axisVal;
        }

        if (newData)
        {
            inputManager.PassInput(data);
            newData = false;
            data.Reset();
        }

    }
}