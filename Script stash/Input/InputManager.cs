﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(InputListener))]
public class InputManager : MonoBehaviour
{
    [Range(0, 10)]
    public int axisCount;
    [Range(0, 20)]
    public int buttonCount;

    public Controller controller;

    public void PassInput(InputData data)
    {
        controller.ConsumeInput(data);
    }

}

public struct InputData
{
    public float[] axis;
    public bool[] buttons;

    public InputData(int axisCount, int buttonCount)
    {
        axis = new float[axisCount];
        buttons = new bool[buttonCount];
    }

    internal void Reset()
    {
        for (int i = 0; i < axis.Length; i++)
        {
            axis[i] = 0f;
        }

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i] = false;
        }
    }
}