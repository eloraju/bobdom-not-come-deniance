﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(InputManager))]
public abstract class InputListener : MonoBehaviour
{
    protected InputManager inputManager;
    protected InputData data;
    protected bool newData;

    private void Awake()
    {
        inputManager = GetComponent<InputManager>();
        data = new InputData(inputManager.axisCount, inputManager.buttonCount);
    }
}

[System.Serializable]
public struct AxisButtons
{
    public KeyCode positivite;
    public KeyCode negative;
}

[System.Serializable]
public struct ActionButtons
{
    public KeyCode button;
    public Action action;
}
