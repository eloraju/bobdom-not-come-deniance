﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Animator))]
public abstract class ThirdPersonController : MonoBehaviour
{
    protected Rigidbody rb;
    protected bool newInstructions, grounded;
    protected Animator anim;
    protected Collider coll;

    public float walkSpeed = 3f;
    public float jumpSpeed = 4f;
    public float turnSpeed = 3f;

    public Weapon[] weapons;
    public int equippedWeapon;

    protected bool hasInput;
    protected bool overrideTurn;

    protected float xVelocity;
    protected float zVelocity;
    protected float yVelocity;

    protected Transform turnOverride;


    public LayerMask groundLayer;
    public float skinWidth = 0.05f;
    public float raycastLenght = 0.1f;
    [Range(0, 180)]
    public float maxClimbAngle = 100f;
    public bool debug;

    private float slopeCorrection;

    public abstract void ConsumeInput(InputData data);
    protected abstract void ResetInput();


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        coll = GetComponent<Collider>();

        //gravity = -(2 * jumpHeight) / Mathf.Pow(timoToApex, 2);
        //jumpVelocity = Mathf.Abs(gravity) * timoToApex;
    }


    private void GroundedCheck()
    {
        Vector3 raycastStart = rb.position;
        raycastStart.y = raycastStart.y + skinWidth;
        RaycastHit hitInfo;

        grounded = Physics.Raycast(raycastStart, Vector3.down, out hitInfo, raycastLenght, groundLayer);
        anim.SetBool("Grounded", grounded);


        if (debug)
        {
            Debug.DrawRay(raycastStart, Vector3.down * raycastLenght, Color.red);
            Debug.Log("Grounded: " + grounded);
        }
    }

    private void Update()
    {
        GroundedCheck();
    }

    private void LateUpdate()
    {
        Debug.Log("Has new instructions: " + newInstructions);
        if (!newInstructions)
        {
            ResetInput();
        }

        newInstructions = false;

        Vector3 move = new Vector3(0, rb.velocity.y + yVelocity, 0);
        Vector3 turn = move;


        Vector3 cameraForward = Camera.main.transform.forward;
        Vector3 cameraRight = Camera.main.transform.right;
        cameraForward.y = 0f;
        cameraRight.y = 0f;

        move += cameraForward * xVelocity;
        move += cameraRight * zVelocity;

        if (!overrideTurn)
        {
           /* Vector3 cameraForward = Camera.main.transform.forward;
            Vector3 cameraRight = Camera.main.transform.right;
            cameraForward.y = 0f;
            cameraRight.y = 0f;

            move += cameraForward * xVelocity;
            move += cameraRight * zVelocity*/
            turn = move;
            turn.y = 0f;
        }
        else
        {
            Vector3 enemyDirectionHasMag = turnOverride.position - rb.position;
            Vector3 enemyDirection = enemyDirectionHasMag / enemyDirectionHasMag.magnitude;
            Vector3 enemyDirectionRight = Quaternion.AngleAxis(90, Vector3.up) * enemyDirection;
            enemyDirection.y = 0;
            enemyDirectionRight.y = 0;

           // move += enemyDirection * xVelocity;
           // move += enemyDirectionRight * zVelocity;
            turn = turnOverride.position - rb.position;
            turn.y = 0f;
        }

        rb.velocity = move;
        if (xVelocity != 0 || zVelocity != 0)
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(turn), Time.deltaTime * turnSpeed);


        if (debug)
        {
            Vector3 start = transform.position;
            start.y = start.y + 0.9f;
            Debug.DrawRay(start, rb.velocity * 0.5f, Color.blue);
        }


    }
}
