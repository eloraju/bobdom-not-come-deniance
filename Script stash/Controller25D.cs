﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Controller25D : MonoBehaviour
{

    public bool debug;

    //Raycast variables
    const float skinWidth = 0.05f;

    [Range(2, 20)]
    public int zRayCount = 3;
    [Range(2, 20)]
    public int yRayCount = 3;
    [Range(2, 20)]
    public int xRayCount = 3;

    public bool useRadialRays = false;

    [Range(2, 360)]
    public int radialRayCount = 8;

    public LayerMask collisionMask;

    public CollisionInfo collisions;

    public float maxClimbAngle;
    public float maxDescentAngle;

    private float zRaySpacing, yRaySpacing, xRaySpacing;
    private float radialRaySpacing;

    [HideInInspector]
    public Collider coll;
    [HideInInspector]
    public PlayerInput input;

    private RaycastOrigins rayOrigins;
    //End of Raycast variables

    private void Awake()
    {
        coll = GetComponent<Collider>();
        CalculateRaySpacing();
    }

    public void Move(Vector3 velocity, PlayerInput input)
    {
        UpdateRayOrigins();
        collisions.Reset();
        this.input = input;

        if (velocity.y < 0)
        {
            DescendSlope(ref velocity);
        }

        if (velocity.z != 0)
            HorizontalCheck(ref velocity);

        if (velocity.y != 0)
            VerticalCheck(ref velocity);

        transform.Translate(velocity);

        //if (debug)
        //{
        //    DrawDebugRaycasts();
        //}
    }

    private void HorizontalCheck(ref Vector3 velocity)
    {
        float directionZ = Mathf.Sign(velocity.z);
        float rayLenght = Mathf.Abs(velocity.z) + skinWidth;
        //Draw either front or back rays
        for (int i = 0; i < xRayCount; i++)
        {
            for (int j = 0; j < yRayCount; j++)
            {
                Vector3 rayOrigin = (directionZ == 1) ? rayOrigins.frontBottomLeft : rayOrigins.backBottomLeft;
                Vector3 direction = (directionZ == 1) ? Vector3.forward : Vector3.back;

                rayOrigin += (Vector3.right * xRaySpacing * i) + (Vector3.up * yRaySpacing * j);
                RaycastHit hitInfo;

                if (Physics.Raycast(rayOrigin, direction, out hitInfo, rayLenght, collisionMask))
                {
                    float slopeAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

                    if (j == 0 && slopeAngle <= maxClimbAngle)
                    {
                        float distanceToSlope = 0;
                        if (slopeAngle != collisions.slopeAngleOld)
                        {
                            distanceToSlope = hitInfo.distance - skinWidth;
                            velocity.z -= distanceToSlope * directionZ;
                        }
                        ClimbSlope(ref velocity, slopeAngle);
                        velocity.z += distanceToSlope * directionZ;
                    }

                    if (!collisions.climbingSlope || slopeAngle > maxClimbAngle)
                    {
                        velocity.z = (hitInfo.distance - skinWidth) * directionZ;
                        rayLenght = hitInfo.distance;

                        collisions.left = directionZ == -1;
                        collisions.right = directionZ == 1;
                    }
                }


                if (debug)
                    Debug.DrawRay(rayOrigin, direction * rayLenght, Color.red);

            }
        }
    }

    private void VerticalCheck(ref Vector3 velocity)
    {
        float directionY = Mathf.Sign(velocity.y);
        float rayLenght = Mathf.Abs(velocity.y) + skinWidth;

        //Draw either top or bottom rays
        for (int i = 0; i < xRayCount; i++)
        {
            for (int j = 0; j < zRayCount; j++)
            {
                Vector3 rayOrigin = (directionY == 1) ? rayOrigins.topBackLeft : rayOrigins.bottomBackLeft;
                Vector3 direction = (directionY == 1) ? Vector3.up : Vector3.down;

                rayOrigin += (Vector3.right * xRaySpacing * i) + (Vector3.forward * zRaySpacing * j);
                RaycastHit hitInfo;

                if (Physics.Raycast(rayOrigin, direction, out hitInfo, rayLenght, collisionMask))
                {
                    velocity.y = (hitInfo.distance - skinWidth) * directionY;
                    rayLenght = hitInfo.distance;

                    collisions.below = directionY == -1;
                    collisions.above = directionY == 1;
                }

                if (collisions.climbingSlope)
                {
                    float directionZ = Mathf.Sign(velocity.z);
                    rayLenght = Mathf.Abs(velocity.z + skinWidth);
                    Vector3 newRayOrigin = ((directionZ == -1) ? rayOrigins.bottomBackLeft : rayOrigins.bottomFrontLeft) + Vector3.up * velocity.y;
                    Vector3 newRayDirection = direction = (directionY == 1) ? Vector3.left : Vector3.right;
                    RaycastHit newHit;
                    if (Physics.Raycast(newRayOrigin, newRayDirection, out newHit, rayLenght, collisionMask))
                    {
                        float slopeAngle = Vector3.Angle(newHit.normal, Vector3.up);
                        if (slopeAngle != collisions.slopeAngle)
                        {
                            velocity.z = (newHit.distance - skinWidth) * directionZ;
                            collisions.slopeAngle = slopeAngle;
                        }
                    }
                }

                if (debug)
                    Debug.DrawRay(rayOrigin, direction * rayLenght, Color.red);
            }
        }
    }

    private void ClimbSlope(ref Vector3 velocity, float slopeAngle)
    {
        float moveDistance = Mathf.Abs(velocity.z);
        float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        //Assume we are jumping
        if (velocity.y <= climbVelocityY)
        {
            velocity.y = climbVelocityY;
            velocity.z = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.z);
            collisions.below = true;
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
        }
    }

    private void DescendSlope(ref Vector3 velocity)
    {
        float directionZ = Mathf.Sign(velocity.z);
        
        RaycastHit hitInfo;
        for (int i = 0; i < xRayCount; i++)
        {
            Vector3 rayOrigin = ((directionZ == 1) ? rayOrigins.bottomBackLeft : rayOrigins.bottomFrontLeft) + Vector3.right * i * xRaySpacing;

            if (Physics.Raycast(rayOrigin, Vector3.down, out hitInfo, Mathf.Infinity, collisionMask))
            {
                float slopeAngle = Vector3.Angle(hitInfo.normal, Vector3.up);
                if (slopeAngle != 0 && slopeAngle <= maxDescentAngle)
                {
                    if (Mathf.Sign(hitInfo.normal.z) == directionZ)
                    {
                        if (hitInfo.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.z))
                        {
                            float moveDistance = Mathf.Abs(velocity.z);
                            float descentVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                            velocity.z = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.z);
                            velocity.y -= descentVelocityY;

                            collisions.slopeAngle = slopeAngle;
                            collisions.descendingSlope = true;
                            collisions.below = true;

                            Debug.DrawRay(rayOrigin, Vector3.down * 2, Color.green);
                        }
                    }
                    break;
                }
            }
            
        }

    }

    private void UpdateRayOrigins()
    {
        Bounds bounds = coll.bounds;
        bounds.Expand(skinWidth * -2);

        rayOrigins.bottomBackLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
        rayOrigins.bottomFrontLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
        rayOrigins.bottomBackRight = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
        rayOrigins.bottomFrontRight = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);

        rayOrigins.frontBottomLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
        rayOrigins.frontBottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);
        rayOrigins.frontTopLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
        rayOrigins.frontTopRight = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);

        rayOrigins.topBackLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z);
        rayOrigins.topFrontLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
        rayOrigins.topBackRight = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
        rayOrigins.topFrontRight = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);

        rayOrigins.backBottomLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
        rayOrigins.backBottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
        rayOrigins.backTopLeft = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
        rayOrigins.backTopRight = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);

        rayOrigins.bottomCenter = rayOrigins.topCenter =bounds.center;
        rayOrigins.bottomCenter.y = bounds.min.y;
        rayOrigins.topCenter.y = bounds.max.y;
        

        Debug.Log(rayOrigins.bottomCenter);
    }

    private void CalculateRaySpacing()
    {
        Bounds bounds = coll.bounds;
        bounds.Expand(skinWidth * -2);

        xRaySpacing = bounds.size.x / (xRayCount - 1);
        yRaySpacing = bounds.size.y / (yRayCount - 1);
        zRaySpacing = bounds.size.z / (zRayCount - 1);

        radialRaySpacing = (Mathf.PI * 2) / radialRayCount;
    }

    private void DrawDebugRaycasts()
    {

        if (useRadialRays)
        {
            //Draw radial rays
            for (int i = 0; i < yRayCount; i++)
            {
                for (int j = 0; j < radialRayCount; j++)
                {
                    float colliderRadius = (coll.bounds.max.x - coll.bounds.min.x) / 2;

                    Vector3 ray = Quaternion.AngleAxis((Mathf.Rad2Deg * radialRaySpacing * j), Vector3.up) * Vector3.forward;
                    Debug.DrawRay(rayOrigins.bottomCenter + (ray * (colliderRadius - skinWidth)) + (Vector3.up * yRaySpacing * i), ray * 1, Color.red);
                }
            }
        }
        else
        {
            //Draw front rays
            for (int i = 0; i < xRayCount; i++)
            {
                for (int j = 0; j < yRayCount; j++)
                {
                    Debug.DrawRay(rayOrigins.frontBottomLeft + (Vector3.right * xRaySpacing * i) + (Vector3.up * yRaySpacing * j), Vector3.forward * 1, Color.red);
                }
            }

            //Draw back rays
            for (int i = 0; i < xRayCount; i++)
            {
                for (int j = 0; j < yRayCount; j++)
                {
                    Debug.DrawRay(rayOrigins.backBottomLeft + (Vector3.right * xRaySpacing * i) + (Vector3.up * yRaySpacing * j), Vector3.back * 1, Color.red);
                }
            }
        }
    }

    struct RaycastOrigins
    {
        public Vector3 bottomBackLeft, bottomFrontLeft, bottomBackRight, bottomFrontRight;
        public Vector3 topBackLeft, topFrontLeft, topBackRight, topFrontRight;
        public Vector3 frontTopLeft, frontBottomLeft, frontTopRight, frontBottomRight;
        public Vector3 backTopLeft, backBottomLeft, backTopRight, backBottomRight;
        public Vector3 bottomCenter, topCenter;
        //Todo: Left and Right face raycast origins.
    }

    public struct CollisionInfo
    {
        public bool above, below, left, right;
        public bool climbingSlope, descendingSlope;
        public float slopeAngle;
        public float slopeAngleOld;

        public void Reset()
        {
            above = below = left = right = false;
            climbingSlope = descendingSlope = false;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }


}
