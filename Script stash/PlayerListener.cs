﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller25D))]
public class PlayerListener : MonoBehaviour
{
    public float moveSpeed = 6;

    [Range(0.0f, 1f)]
    public float smoothingGround;

    [Range(0.0f, 1f)]
    public float smoothingAir;

    public float timeToJumpApex;
    public float maxJumpHeight;
    public float minJumpHeight;

    public PlayerAction[] actions;
    PowerSlam powerSlam;

    
    float zVelocitySmoothing;

    [HideInInspector]
    public PlayerInput input;
    [HideInInspector]
    public Controller25D controller;
    [HideInInspector]
    public bool applyGravity = true;
    [HideInInspector]
    public float gravity;
    [HideInInspector]
    public float maxJumpVelocity;
    [HideInInspector]
    public float minJumpVelocity;
    [HideInInspector]
    public Vector3 velocity;

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<Controller25D>();
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
        input = new PlayerInput();
        powerSlam = GetComponent<PowerSlam>();

    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();
        applyGravity = true;

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }

        if (input.jump && controller.collisions.below)
        {
            velocity.y = maxJumpVelocity;
        }

        if (input.jumpReleased)
        {
            if (velocity.y > minJumpVelocity)
            {
                velocity.y = minJumpVelocity;
            }
        }

        float targetVelocityZ = input.movementZ * moveSpeed;
        velocity.z = Mathf.SmoothDamp(velocity.z, targetVelocityZ, ref zVelocitySmoothing, (controller.collisions.below ? smoothingGround : smoothingAir));

        if (powerSlam.inUse)
        {
            powerSlam.UpdateAbility();
        }

        if (applyGravity)
            velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime, input);
    }

    private void CheckInput()
    {
        input.Reset();
        for (int i = 0; i < actions.Length; i++)
        {
            if (Input.GetKeyDown(actions[i].button))
            {
                switch (actions[i].action)
                {
                    case Action.Jump:
                        {
                            input.jump = true;
                            break;
                        }

                    case Action.PowerSlam:
                        {
                            if (!controller.collisions.below)
                            {
                                //input.powerSlam = true;
                               // powerSlam.StartAbility(this);
                            }
                            break;
                        }

                    case Action.ResetPosition:
                        {
                            transform.position = new Vector3(0, 1, 0);
                            break;
                        }
                }
            }

            if (Input.GetKeyUp(actions[i].button))
            {
                switch (actions[i].action)
                {
                    case Action.Jump:
                        {
                            input.jumpReleased = true;
                            break;
                        }
                }
            }

            if (Input.GetKey(actions[i].button))
            {
                switch (actions[i].action)
                {
                    case Action.MoveRight:
                        {
                            input.movementZ = 1;
                            break;
                        }

                    case Action.MoveLeft:
                        {
                            input.movementZ = -1;
                            break;
                        }
                }
            }
        }
    }
}

[System.Serializable]
public struct PlayerAction
{
    public Action action;
    public KeyCode button;
}

public struct PlayerInput
{
    public float movementZ;
    public bool jump, powerSlam, jumpReleased;

    public void Reset()
    {
        movementZ = 0;
        jump = false;
        powerSlam = false;
        jumpReleased = false;
    }
}

[System.Serializable]
public enum Action { Attack, Jump, MoveLeft, MoveRight, MoveFront, MoveBack, Dodge, Parry, Roll, PowerSlam, ResetPosition }
