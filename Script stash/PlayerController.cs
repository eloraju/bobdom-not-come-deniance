﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float moveFB, moveLR;

    public float moveSpeed, sprintFactor, rotationSpeed, dodgeFactor, rollFactor, dodgeTimerInit, rollTimer;

    private float dodgeTimer;

    private bool dodgePressed, dodgin, rolling, sprinting;

    public bool moving;

    private Vector3 moveVector;

    private void Update()
    {
        //Hello
        registerInputs();

        if ((dodgePressed || dodgin) && moving)
        {
            Dodge();
        }
        else
        {
            Move();

        }
        moving = false;
    }

    private void registerInputs()
    {
        sprinting = Input.GetButton("Sprint");
        moveFB = sprinting ? Input.GetAxis("Vertical") * moveSpeed * sprintFactor : Input.GetAxis("Vertical") * moveSpeed;
        moveLR = sprinting ? Input.GetAxis("Horizontal") * moveSpeed * sprintFactor : Input.GetAxis("Horizontal") * moveSpeed;
        dodgePressed = Input.GetButtonDown("Dodge");
        if (dodgePressed)
            dodgin = true;
        moving = true;
    }

    public void Move()
    {
        float cameraRotation = Camera.main.transform.rotation.eulerAngles.y;

        moveVector = new Vector3(moveLR, 0, moveFB);

        moveVector = transform.rotation * moveVector;
        transform.position += moveVector * Time.deltaTime;
        if (moveFB != 0 || moveLR != 0)
        {
            Quaternion turnAngle = Quaternion.Euler(0, cameraRotation, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, turnAngle, Time.deltaTime * rotationSpeed);
        }
    }

    public void Dodge()
    {
        if (dodgin)
        {
            dodgeTimer -= Time.deltaTime;
            Vector3 dodgeVector = moveVector * dodgeFactor;
            transform.position += dodgeVector * Time.deltaTime;
            if (dodgeTimer <= 0)
            {
                dodgin = false;
                dodgeTimer = dodgeTimerInit;
            }
        }
    }
}
