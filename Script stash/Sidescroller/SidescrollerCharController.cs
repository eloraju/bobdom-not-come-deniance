﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[RequireComponent(typeof(Collider))]
//public class SidescrollerCharController : CollisionController
//{


//    [HideInInspector]
//    public PlayerController2d.PlayerInput input;

//    public void Move(Vector3 moveVector, PlayerController2d.PlayerInput input)
//    {
//        this.input = input;
//        UpdateCollisions();

//        //If the player is going down, we can check wheter we're going down a slope or not
//        //if (moveVector.y < 0 && moveVector.z != 0)
//        //{
//        //    DescendingSlopeCheck(ref moveVector);
//        //    //Debug.Log("AFTER DES SLOPE\n Y: " + moveVector.y + "\nZ: " + moveVector.z);
//        //}


//        if (moveVector.z != 0)
//        {
//            HorizontalCheck(ref moveVector);
//            //Debug.Log("AFTER HOR\n Y: " + moveVector.y + "\nZ: " + moveVector.z);
//        }

//        //Just check for grounded state...
//        if (moveVector.y <= 0)
//            groundedCheck(ref moveVector);

//        //Check for collisions above or below the player
//        if (moveVector.y != 0)
//        {
//            //VerticalCheck(ref moveVector);
//            //Debug.Log("AFTER VER\n Y: " + moveVector.y + "\nZ: " + moveVector.z);
//        }

//        transform.Translate(moveVector);
//    }
//    private void DescendingSlopeCheck(ref Vector3 moveVector)
//    {
//        float directionZ = Mathf.Sign(moveVector.z);

//        RaycastHit hitInfo;
//        float rayLenght = 2;
//        for (int i = 0; i < xRayCount; i++)
//        {
//            Vector3 rayOrigin = ((directionZ == 1) ? rayOrigins.bottomBackLeft : rayOrigins.bottomFrontLeft) + Vector3.right * i * xRaySpacing;

//            if (Physics.Raycast(rayOrigin, Vector3.down, out hitInfo, rayLenght, collisionMask))
//            {
//                Debug.Log("Slope descending hit detected.");
//                float slopeAngle = Vector3.Angle(hitInfo.normal, Vector3.up);
//                if (slopeAngle != 0 && slopeAngle <= maxDescentAngle)
//                {
//                    Debug.Log("Slope descending angle is A OK ");

//                    if (Mathf.Sign(hitInfo.normal.z) == directionZ)
//                    {

//                        Debug.Log("Slope descending direction is also a ok");
//                        float distToSlope = hitInfo.distance - skinWidth;
//                        float yDistToSlope = Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveVector.z);

//                        Debug.Log("To slope: " + distToSlope + " - Y move dist: " + yDistToSlope);

//                        if (distToSlope <= yDistToSlope)
//                        {
//                            Debug.Log("Slope descending slope distance is less or eaqual to our Y distance to the slope");

//                            float moveDistance = Mathf.Abs(moveVector.z);
//                            float descentVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
//                            moveVector.z = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveVector.z);
//                            moveVector.y -= descentVelocityY;

//                            collisions.slopeAngle = slopeAngle;
//                            collisions.descendingSlope = true;
//                            collisions.grounded = true;

//                            rayLenght = hitInfo.distance - skinWidth;

//                        }
//                    }

//                }
//            }
//            Debug.DrawRay(rayOrigin, Vector3.down, Color.green);

//        }
//    }

//    private void HorizontalCheck(ref Vector3 moveVector)
//    {
//        float directionZ = Mathf.Sign(moveVector.z);
//        float rayLenght = Mathf.Abs(moveVector.z) + skinWidth;
//        //Draw either front or back rays
//        for (int i = 0; i < xRayCount; i++)
//        {
//            for (int j = 0; j < yRayCount; j++)
//            {
//                Vector3 rayOrigin = (directionZ == 1) ? rayOrigins.frontBottomLeft : rayOrigins.backBottomLeft;
//                Vector3 direction = (directionZ == 1) ? Vector3.forward : Vector3.back;

//                rayOrigin += (Vector3.right * xRaySpacing * i) + (Vector3.up * yRaySpacing * j);
//                RaycastHit hitInfo;

//                if (Physics.Raycast(rayOrigin, direction, out hitInfo, rayLenght, collisionMask))
//                {
//                    float slopeAngle = Vector3.Angle(hitInfo.normal, Vector3.up);

//                    if (j == 0 && slopeAngle <= maxClimbAngle)
//                    {
//                        float distanceToSlope = 0;
//                        if (slopeAngle != collisions.slopeAngleOld)
//                        {
//                            distanceToSlope = hitInfo.distance - skinWidth;
//                            moveVector.z -= distanceToSlope * directionZ;
//                        }
//                        ClimbSlope(ref moveVector, slopeAngle);
//                        moveVector.z += distanceToSlope * directionZ;
//                    }

//                    if (!collisions.climbingSlope || slopeAngle > maxClimbAngle)
//                    {
//                        moveVector.z = (hitInfo.distance - skinWidth) * directionZ;
//                        rayLenght = hitInfo.distance;

//                        if (collisions.climbingSlope)
//                        {
//                            moveVector.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveVector.z);
//                        }

//                    }
//                }
//                if (debug)
//                    Debug.DrawRay(rayOrigin, direction, Color.red);
//            }
//        }
//    }

//    private void ClimbSlope(ref Vector3 moveVector, float slopeAngle)
//    {
//        float moveDistance = Mathf.Abs(moveVector.z);
//        float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

//        //Assume we are jumping
//        if (moveVector.y <= climbVelocityY)
//        {
//            moveVector.y = climbVelocityY;
//            moveVector.z = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveVector.z);
//            collisions.grounded = true;
//            collisions.climbingSlope = true;
//            collisions.slopeAngle = slopeAngle;
//        }
//    }

//    private void groundedCheck(ref Vector3 moveVector)
//    {

//        float directionY = Mathf.Sign(moveVector.y);
//        float rayLenght = Mathf.Abs(moveVector.y) + skinWidth;

//        for (int i = 0; i < xRayCount; i++)
//        {
//            for (int j = 0; j < zRayCount; j++)
//            {
//                Vector3 rayOrigin = rayOrigins.bottomBackLeft;

//                rayOrigin += (Vector3.right * xRaySpacing * i) + (Vector3.forward * zRaySpacing * j);
//                RaycastHit hitInfo;

//                if (debug)
//                    Debug.DrawRay(rayOrigin, Vector3.down, Color.red);

//                if (Physics.Raycast(rayOrigin, Vector3.down, out hitInfo, rayLenght, collisionMask))
//                {
//                    moveVector.y = (hitInfo.distance - skinWidth) * directionY;
//                    rayLenght = hitInfo.distance;

//                    //if (collisions.climbingSlope)
//                    //{
//                    //    moveVector.z = moveVector.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveVector.z);
//                    //}

//                    collisions.grounded = true;
//                }

//                //if (collisions.climbingSlope)
//                //{
//                //    float directionZ = Mathf.Sign(moveVector.z);
//                //    rayLenght = Mathf.Abs(moveVector.z + skinWidth);
//                //    Vector3 newRayOrigin = ((directionZ == -1) ? rayOrigins.bottomBackLeft : rayOrigins.bottomFrontLeft) + Vector3.up * moveVector.y;
//                //    Vector3 newRayDirection = direction = (directionY == 1) ? Vector3.left : Vector3.right;
//                //    RaycastHit newHit;
//                //    if (Physics.Raycast(newRayOrigin, newRayDirection, out newHit, rayLenght, collisionMask))
//                //    {
//                //        float slopeAngle = Vector3.Angle(newHit.normal, Vector3.up);
//                //        if (slopeAngle != collisions.slopeAngle)
//                //        {
//                //            moveVector.z = (newHit.distance - skinWidth) * directionZ;
//                //            collisions.slopeAngle = slopeAngle;
//                //        }
//                //    }
//                //}
//            }
//        }
//    }

//    private void MovingUpCheck(ref Vector3 moveVector)
//    {

//        float directionY = Mathf.Sign(moveVector.y);
//        float rayLenght = Mathf.Abs(moveVector.y) + skinWidth;

//        //Draw either top or bottom rays
//        for (int i = 0; i < xRayCount; i++)
//        {
//            for (int j = 0; j < zRayCount; j++)
//            {
//                Vector3 rayOrigin = rayOrigins.topBackLeft;
//                Vector3 direction = Vector3.up;

//                rayOrigin += (Vector3.right * xRaySpacing * i) + (Vector3.forward * zRaySpacing * j);
//                RaycastHit hitInfo;

//                if (debug)
//                    Debug.DrawRay(rayOrigin, direction, Color.red);

//                if (Physics.Raycast(rayOrigin, direction, out hitInfo, rayLenght, collisionMask))
//                {
//                    moveVector.y = (hitInfo.distance - skinWidth) * directionY;
//                    rayLenght = hitInfo.distance;

//                    collisions.above = true;
//                }
//            }
//        }
//    }

//    private void VerticalCheck(ref Vector3 moveVector)
//    {
//        float directionY = Mathf.Sign(moveVector.y);
//        float rayLenght = Mathf.Abs(moveVector.y) + skinWidth;

//        //Draw either top or bottom rays
//        for (int i = 0; i < xRayCount; i++)
//        {
//            for (int j = 0; j < zRayCount; j++)
//            {
//                Vector3 rayOrigin = (directionY == 1) ? rayOrigins.topBackLeft : rayOrigins.bottomBackLeft;
//                Vector3 direction = (directionY == 1) ? Vector3.up : Vector3.down;

//                rayOrigin += (Vector3.right * xRaySpacing * i) + (Vector3.forward * zRaySpacing * j);
//                RaycastHit hitInfo;

//                if (debug)
//                    Debug.DrawRay(rayOrigin, direction, Color.red);

//                if (Physics.Raycast(rayOrigin, direction, out hitInfo, rayLenght, collisionMask))
//                {
//                    moveVector.y = (hitInfo.distance - skinWidth) * directionY;
//                    rayLenght = hitInfo.distance;

//                    if (collisions.climbingSlope)
//                    {
//                        moveVector.z = moveVector.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveVector.z);
//                    }

//                    collisions.grounded = directionY <= 0;
//                    collisions.above = directionY == 1;
//                }

//                if (collisions.climbingSlope)
//                {
//                    float directionZ = Mathf.Sign(moveVector.z);
//                    rayLenght = Mathf.Abs(moveVector.z + skinWidth);
//                    Vector3 newRayOrigin = ((directionZ == -1) ? rayOrigins.bottomBackLeft : rayOrigins.bottomFrontLeft) + Vector3.up * moveVector.y;
//                    Vector3 newRayDirection = direction = (directionY == 1) ? Vector3.left : Vector3.right;
//                    RaycastHit newHit;
//                    if (Physics.Raycast(newRayOrigin, newRayDirection, out newHit, rayLenght, collisionMask))
//                    {
//                        float slopeAngle = Vector3.Angle(newHit.normal, Vector3.up);
//                        if (slopeAngle != collisions.slopeAngle)
//                        {
//                            moveVector.z = (newHit.distance - skinWidth) * directionZ;
//                            collisions.slopeAngle = slopeAngle;
//                        }
//                    }
//                }
//            }
//        }
//    }

//}
