﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RaycastController : MonoBehaviour
{
    [HideInInspector]
    public new Collider collider;

    public CollisionInfo collisions;

    float rayCount;

    public float skinWidth = .015f;
    float raySpacing = .1f;

    public LayerMask collisionMask;
    public float maxDescentAngle = 50;
    public float maxClimbAngle = 50;

    public bool debug;

    protected RaycastOrigins rayOrigins;
    protected float zRaySpacing, yRaySpacing, xRaySpacing;
    protected int zRayCount;
    protected int yRayCount;
    protected int xRayCount;

    private void Awake()
    {
        collider = GetComponent<Collider>();
        collisions = new CollisionInfo();
    }

    public virtual void Start()
    {
        CalculateRayCount();
    }

    public void UpdateCollisions(ref Vector3 moveVector)
    {
        UpdateRayOrigins();
        collisions.Reset();
    }

    private void UpdateRayOrigins()
    {
        Bounds bounds = collider.bounds;
        bounds.Expand(skinWidth * -2);

        rayOrigins.bottomBackLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
        rayOrigins.bottomFrontLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
        rayOrigins.bottomBackRight = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
        rayOrigins.bottomFrontRight = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);

        rayOrigins.frontBottomLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.max.z);
        rayOrigins.frontBottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.max.z);
        rayOrigins.frontTopLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
        rayOrigins.frontTopRight = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);

        rayOrigins.topBackLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.min.z);
        rayOrigins.topFrontLeft = new Vector3(bounds.min.x, bounds.max.y, bounds.max.z);
        rayOrigins.topBackRight = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
        rayOrigins.topFrontRight = new Vector3(bounds.max.x, bounds.max.y, bounds.max.z);

        rayOrigins.backBottomLeft = new Vector3(bounds.min.x, bounds.min.y, bounds.min.z);
        rayOrigins.backBottomRight = new Vector3(bounds.max.x, bounds.min.y, bounds.min.z);
        rayOrigins.backTopLeft = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);
        rayOrigins.backTopRight = new Vector3(bounds.max.x, bounds.max.y, bounds.min.z);

        rayOrigins.bottomCenter = rayOrigins.topCenter = bounds.center;
        rayOrigins.bottomCenter.y = bounds.min.y;
        rayOrigins.topCenter.y = bounds.max.y;
    }

    private void CalculateRayCount()
    {
        Bounds bounds = collider.bounds;
        bounds.Expand(skinWidth * -2);

        float boundsWidth, boundsDepth, boundsHeight;
        boundsWidth = bounds.size.z;
        boundsDepth = bounds.size.x;
        boundsHeight = bounds.size.y;

        yRayCount = Mathf.RoundToInt(boundsHeight / raySpacing);
        xRayCount = Mathf.RoundToInt(boundsDepth / raySpacing);
        zRayCount = Mathf.RoundToInt(boundsWidth / raySpacing);

        yRaySpacing = bounds.size.y / (yRayCount - 1);
        xRaySpacing = bounds.size.x / (xRayCount - 1);
        zRaySpacing = bounds.size.z / (zRayCount - 1);
    }


    protected struct RaycastOrigins
    {
        public Vector3 bottomBackLeft, bottomFrontLeft, bottomBackRight, bottomFrontRight;
        public Vector3 topBackLeft, topFrontLeft, topBackRight, topFrontRight;
        public Vector3 frontTopLeft, frontBottomLeft, frontTopRight, frontBottomRight;
        public Vector3 backTopLeft, backBottomLeft, backTopRight, backBottomRight;
        public Vector3 bottomCenter, topCenter;
    }

    [System.Serializable]
    public struct CollisionInfo
    {
        public bool grounded, above;

        public bool climbingSlope, descendingSlope;

        public float slopeAngle, slopeAngleOld;

        public void Reset()
        {
            grounded = above = false;
            climbingSlope = descendingSlope = false;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }
}
